using System.Collections;
using Modding;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace stupidtimermod
{
    public class stupid : MonoBehaviour
    {
        private void Start()
        {
            UnityEngine.SceneManagement.SceneManager.activeSceneChanged += stopTimerCheck;
        }
        
        private void OnDestroy()
        {
            UnityEngine.SceneManagement.SceneManager.activeSceneChanged -= stopTimerCheck;
            //timer.isRunning = false;
        }

        private void stopTimerCheck(Scene from, Scene to)
        {
            if (from.name == "Menu_Title")
            {
                timer.isRunning = true;
            }
            
            if (to.name.Contains("Cinematic_Ending"))
            {
                timer.isRunning = false;
            }
        }
    }
}