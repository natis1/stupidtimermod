using System;
using System.Globalization;
using System.Linq;
using Modding;
using UnityEngine;
using UnityEngine.UI;

namespace stupidtimermod
{
    public class timer : MonoBehaviour
    {
        public static double currentTime = 0.0;
        public static bool isRunning = false;
        private Text textObj;
        
        private void Start()
        {
            textObj = gameObject.GetComponent<Text>();
            textObj.color = Color.white;
            textObj.font = CanvasUtil.TrajanBold;
            textObj.text = getTimeInCleanFormat(currentTime);
            textObj.fontSize = 50;
            textObj.CrossFadeAlpha(1f, 0f, false);
        }

        private void Update()
        {
            if (isRunning && !GameManager.instance.IsInSceneTransition)
            {
                currentTime += Time.unscaledDeltaTime;
            }
            textObj.text = getTimeInCleanFormat(currentTime);
        }


        private static string getTimeInCleanFormat(double time)
        {
            string milliseconds = (time - Math.Truncate(time)).ToString(CultureInfo.CurrentCulture);
            milliseconds = milliseconds.Substring(1, milliseconds.Length - 1);
            if (milliseconds.Length > 3)
            {
                milliseconds = milliseconds.Substring(0, 3);
            } else if (milliseconds.Length < 3)
            {
                for (int i = (3 - milliseconds.Length); i > 0; i--)
                {
                    milliseconds += "0";
                }
            }
            
            string seconds = (((int) time) % 60).ToString();
            if (seconds.Length == 1)
            {
                seconds = "0" + seconds;
            }
            string minutes = (((int) time) / 60).ToString();
            if (minutes.Length == 1)
            {
                minutes = "0" + minutes;
            }
            string hours = (((int) time) / 3600).ToString();
            return (hours + ":" + minutes + ":" + seconds + milliseconds);
        }
    }
}